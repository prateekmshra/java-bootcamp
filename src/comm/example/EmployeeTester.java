package comm.example;

public class EmployeeTester {
    public static void main(String[] args)
    {
        Employee employee=new Employee();
        employee.setFirstName("John");
        employee.setLastName("Doe");
        employee.setEmail("John@gmail.com");
        System.out.println("\nbefore calling modify method");
        employee.getDetails();
        Employee employee1=employee.modifyEmployee(employee);
        System.out.println("\nafter calling modify method");
        employee.getDetails();
    }

}

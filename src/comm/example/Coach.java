package comm.example;
import java.util.UUID;
public class Coach {
    private String coachName;
    private String coachType;
    private String coachID;

    public Coach() {
        this(UUID.randomUUID().toString());
        System.out.println("under default constructor");
    }

    public Coach(String coachID) {
        this("John","Permanent");
        this.coachID = coachID;
        System.out.println("under single parameter constructor");
    }

    public Coach(String coachName, String coachType) {
        this(coachName,coachType,UUID.randomUUID().toString());
        this.coachName = coachName;
        this.coachType = coachType;
        System.out.println("Under double parameter constructor");
    }

    public Coach(String coachName, String coachType, String coachID) {

        this.coachName = coachName;
        this.coachType = coachType;
        this.coachID = coachID;
        System.out.println("under final constructor");
    }

    public String getDetails()
    {
        return "ID: " +coachID+" Name: "+coachName+" type: "+coachType;
    }
}
